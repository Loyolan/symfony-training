<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;

/**
 * @Route("/api", name="api_")
 */
class UserController extends AbstractController
{
    // GET ALL USERS
    /**
     * @Route("/users", name="all_user", methods={"get"})
     */
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $users = $doctrine
            ->getRepository(User::class)
            ->findAll();
        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'password' => $user->getPassword(),
                'email' => $user->getEmail(),
                'fullname' => $user->getFullname(),
                'role' => $user->getRole()
            ];
        }
        return $this->json($data);
    }

    // ADD AN USER
    /**
     * @Route("/users", name="add_user", methods={"POST"})
     */
    public function create(ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordHasher, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $user = new User();
        $req = json_decode($request->getContent(), true);
        $hashedPassword = $passwordHasher->hashPassword($user, $req['password']);
        $user->setName($req['name']);
        $user->setPassword($hashedPassword);
        $user->setEmail($req['email']);
        $user->setFullname($req['fullname']);
        $user->setRole($req['role']);

        $entityManager->persist($user);
        $entityManager->flush();

        $data = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'fullname' => $user->getFullname(),
            'role' => $user->getRole()
        ];
        return $this->json($req);
    }

    // GET AN USER
    /**
     * @Route("/users/{id}", name="one_user", methods={"get"})
     */
    public function show(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $user = $doctrine->getRepository(User::class)->find($id);
        if(!$user) {
            return $this->json('No user found for id '. $id, 404);
        }
        $data = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'password' => $user->getPassword(),
            'email' => $user->getEmail(),
            'fullname' => $user->getFullname(),
            'role' => $user->getRole()
        ];
        return $this->json($data);
    }

    // UPDATE AN USER
    /**
     * @Route("/users/{id}", name="update_user", methods={"put", "patch"})
     */
    public function update(ManagerRegistry $doctrine, Request $request, int $id)
    {
        $entityManager = $doctrine->getManager();
        $user = $doctrine->getRepository(User::class)->find($id);
        if(!$user) {
            return $this->json('No user found for id '. $id, 404);
        }
        $req = json_decode($request->getContent(), true);
        $user->setName($req['name']);
        $user->setEmail($req['email']);
        $user->setFullname($req['fullname']);
        $user->setRole($req['role']);
        $entityManager->flush();

        $data = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'password' => $user->getPassword(),
            'email' => $user->getEmail(),
            'fullname' => $user->getFullname(),
            'role' => $user->getRole()
        ];
        return $this->json($data);
    }

    // DELETE AN USER
    /**
     * @Route("/users/{id}", name="delete_user", methods={"delete"})
     */
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $user = $doctrine->getRepository(User::class)->find($id);
        if(!$user) {
            return $this->json('No user found for id '. $id, 404);
        }

        $entityManager->remove($user);
        $entityManager->flush();

        return $this->json('Deleted user successfully whith id '. $id);
    }
}
